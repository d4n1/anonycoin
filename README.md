# Anonycoin

Anonycoin is a Bitcoin simulator with anonymity throught group signature.

## Requirements
* ecdsa==0.13
* pep8==1.7.0
* pyfiglet==0.7.4
* pyzmq==15.2.0
* requests==2.9.1

For more requeriments read requeriments file.

## Documentation
Read a HTML files in documentation folder.

## Instalation (pyvenv or virtualenv)
```
pyvenv .env
. .env/bin/activate
pip install -r requeriments
```

## Runing
```
cd anonycoin
python anonycoin.py
```

## Usage
* [*] Exit
* [1] Simulator
* [2] De-anonymity
* [3] Price
* [4] About

## Notes
* Anonycoin not generate a Coinbase (first Bitcoin minied)
* Using fibonacci to simulate POW
* Wallet, transaction, script, network, blockchain simplified

## Code style
* pep8 --show-source --show-pep8

## Todo
* Testnet
