## time

anonycoin <- read.csv(file="anonycoin-time.csv")
bitcoin <- read.csv(file="bitcoin-time.csv")
max_y <- max(anonycoin[,1])
plot_colors <- c("red", "blue")
pdf("analysis-time.pdf")
plot(anonycoin[,1], col=plot_colors[1], ylim=c(0, max_y), xlab="Transação", ylab="Tempo")
box()
lines(bitcoin[,1], pch=22, lty=8, col=plot_colors[2])
title(main="Desempenho Bitcoin e Anonycoin", col.main="red", font.main=4)
legend(1, 1, c("Anonycoin", "Bitcoin"), cex=0.8, col=plot_colors, pch=21:23,  lty=1:3)

## Statistics anonycoin time
summary(anonycoin)
# Min.    1st Qu. Median  Mean    3rd Qu. Max. 
# 0.3198  0.3271  0.3320  0.3361  0.3357  0.4862 
mean(anonycoin)
# 0.336092
var(anonycoin)
# 0.0004450458
sd(anonycoin)
# 0.02109611
median(anonycoin)
# 0.332019
shapiro.test(rnorm(497, mean=0.336092, sd=0.02109611))
# Shapiro-Wilk normality test
# data:  rnorm(497, mean = 0.336092, sd = 0.02109611)
# W = 0.99076, p-value = 0.003332

## Statistics Bitcoin time
summary(bitcoin)
# Min.    1st Qu. Median  Mean    3rd Qu. Max. 
# 0.1592  0.1647  0.1672  0.1680  0.1691  0.2234 
mean(bitcoin)
# 0.1680369
var(bitcoin)
# 4.591033e-05
sd(bitcoin)
# 0.006775716
median(bitcoin)
# 0.167183
shapiro.test(rnorm(497, mean=0.1680369, sd=0.006775716))
# Shapiro-Wilk normality test
# data:  rnorm(497, mean = 0.1680369, sd = 0.006775716)
# W = 0.994, p-value = 0.04687


## Student's t distribution with 2 samples distincts
var.test(bitcoin, anonycoin)
# F test to compare two variances
# data:  bitcoin and anonycoin
# F = 0.10316, num df = 496, denom df = 496, p-value < 2.2e-16
# alternative hypothesis: true ratio of variances is not equal to 1
# 95 percent confidence interval:
# 0.08649235 0.12303647
# sample estimates:
# ratio of variances 
# 0.1031587 
qf(0.95, 496, 496)
# 1.159344
t.test(bitcoin, anonycoin)
# Welch Two Sample t-test
# data:  bitcoin and anonycoin
# t = -169.09, df = 597.26, p-value < 2.2e-16
# alternative hypothesis: true difference in means is not equal to 0
# 95 percent confidence interval:
# -0.1700070 -0.1661031
# sample estimates:
# mean of x mean of y 
# 0.1680369 0.3360920 
qt(0.975, 597.26)
# 1.963944

###############################################################################

## Tracking
anonycoin <- read.csv(file="anonycoin-track.csv")
bitcoin <- read.csv(file="bitcoin-track.csv")
max_y <- max(anonycoin[,1])
plot_colors <- c("red", "blue")
pdf("analysis-track.pdf")
plot(anonycoin[,1], col=plot_colors[1], ylim=c(0, max_y), xlab="Cliente", ylab="Anonimato")
box()
lines(bitcoin[,1], pch=22, lty=8, col=plot_colors[2])
title(main="Anonimato Bitcoin e Anonycoin", col.main="red", font.main=4)
legend(1, 0.8, c("Anonycoin", "Bitcoin"), cex=0.8, col=plot_colors, pch=21:23,  lty=1:3)


track_file <- read.csv(file="track.csv")
bitcoin <- c(track_file$Bitcoin)
anonycoin <- c(track_file$Anonycoin)

## Plot track
track <- read.csv(file="track_file.csv")
max_y <- max(track)
plot_colors <- c("blue", "red")
pdf("analysis-track.pdf")
plot(bitcoin, type="o", col=plot_colors[1], ylim=c(0, max_y), xlab="Transaction", ylab="Track")
box()
lines(anonycoin, type="o", pch=22, lty=2, col=plot_colors[2])
title(main="Bitcoin and Anonycoin Track", col.main="red", font.main=4)
legend(1, max_y, names(track_file), cex=0.8, col=plot_colors, pch=21:23,  lty=1:3)

## Statistics Bitcoin track
summary(bitcoin)
# Min.    1st Qu. Median  Mean    3rd Qu. Max. 
# 0.00    5.00    6.00    6.07    8.00   13.00 
mean(bitcoin)
# 6.070423
var(bitcoin)
# 5.60995
sd(bitcoin)
# 2.368533
median(bitcoin)
# 6
shapiro.test(rnorm(497, mean=6.070423, sd=2.368533))
# Shapiro-Wilk normality test
data:  rnorm(497, mean = 6.070423, sd = 2.368533)
W = 0.99346, p-value = 0.02995

## Statistics anonycoin track
summary(anonycoin)
# Min.    1st Qu. Median  Mean    3rd Qu. Max. 
# 0       0       0       0       0       0
mean(anonycoin)
# 0
var(anonycoin)
# 0
sd(anonycoin)
# 0
median(anonycoin)
# 0
shapiro.test(rnorm(497, mean=0, sd=0.01))
# Shapiro-Wilk normality test
# data:  rnorm(497, mean = 0, sd = 0.01)
# W = 0.99375, p-value = 0.03803

## Student's t distribution with 2 samples distincts
var.test(bitcoin, anonycoin)
# F test to compare two variances
# data:  bitcoin and anonycoin
# F = Inf, num df = 496, denom df = 496, p-value < 2.2e-16
# alternative hypothesis: true ratio of variances is not equal to 1
# 95 percent confidence interval:
# Inf Inf
# sample estimates:
# ratio of variances 
# Inf 
qf(0.95, 496, 496)
# 1.159344
t.test(bitcoin, anonycoin)
# Welch Two Sample t-test
# data:  bitcoin and anonycoin
# t = 57.137, df = 496, p-value < 2.2e-16
# alternative hypothesis: true difference in means is not equal to 0
# 95 percent confidence interval:
# 5.861680 6.279165
# sample estimates:
# mean of x mean of y 
# 6.070423  0.000000 
qt(0.975, 496)
# 1.964758


#####################################################################

## Audit
on <- read.csv(file="anonycoin-audit-on.csv")
off <- read.csv(file="anonycoin-audit-off.csv")
max_y <- max(on[,1])
plot_colors <- c("red", "blue")
pdf("analysis-audit.pdf")
plot(on[,1], col=plot_colors[1], ylim=c(0, max_y), xlab="Cliente", ylab="Auditoria")
box()
lines(off[,1], pch=22, lty=8, col=plot_colors[2])
title(main="Auditoria Anonycoin", col.main="red", font.main=4)
legend(1, 0.8, c("Ligado", "Desligado"), cex=0.8, col=plot_colors, pch=21:23,  lty=1:3)


######################################################################3

## statistic
anonycoin <- read.csv(file="anonycoin-time.csv")
bitcoin <- read.csv(file="bitcoin-time.csv")

lmfunction <- lm(anonycoin$time[0:4999]~bitcoin$time[0:4999])

summary(lmfunction)

#Call:
#lm(formula = anonycoin$time[0:4999] ~ bitcoin$time[0:4999])

#Residuals:
#     Min       1Q   Median       3Q      Max 
#-1.31762 -0.19106  0.00052  0.18348  2.71269 

#Coefficients:
#                     Estimate Std. Error t value Pr(>|t|)    
#(Intercept)          4.909518   0.042577   115.3   <2e-16 ***
#bitcoin$time[0:4999] 0.847132   0.008141   104.1   <2e-16 ***
#---
#Signif. codes:  0 *** 0.001 ** 0.01 * 0.05 . 0.1   1

#Residual standard error: 0.2736 on 4997 degrees of freedom
#Multiple R-squared:  0.6843,	Adjusted R-squared:  0.6842 
#F-statistic: 1.083e+04 on 1 and 4997 DF,  p-value: < 2.2e-16

shapiro.test(rstudent(lmfunction))

#Shapiro-Wilk normality test

#data:  rstudent(lmfunction)
#W = 0.98593, p-value < 2.2e-16

pdf("lm-analysis.pdf")
plot(rstudent(lmfunction) ~ fitted(lmfunction), pch=19, xlab="Filtrado", ylab="Residual")
abline(h=0, lty=2)

pdf("lm-graph.pdf")
plot(anonycoin$time~bitcoin$time, xlab="Bitcoin", ylab="Anonycoin")
abline(lmfunction, lty=2)


## other

anonycoin <- read.csv(file="anonycoin-time.csv")
bitcoin <- read.csv(file="bitcoin-time.csv")

reg = lm(anonycoin$time~bitcoin$time)

summary(reg)

fitted(reg)

resid(reg)

shapiro.test(resid(reg)[0:5000])

pdf("qq-analysis.pdf")
qqnorm(resid(reg), xlab="Quantis teóricos", ylab="Quantis amostrais", main="Grafico Q-Q dos Resíduos")
qqline(resid(reg),col=2)

pdf("rv-analysis.pdf")
plot(fitted(reg), resid(reg), xlab="Valores ajustados", ylab="Residuos", main="Residuos x Valores Ajustados")
abline(h=0,lty=2,col=2)

pdf("ro-analysis.pdf")
plot(resid(reg), type="o", xlab="Ordem", ylab="Residuos",main="Residuos x Ordem")
abline(h=0,lty=2,col=2)

pdf("c-analysis.pdf")
plot(anonycoin$time, bitcoin$time, xlab="Anonycoin", ylab="Bitcoin", main="Curva Analitica", abline(reg,col=2))