## File track
track_file <- read.csv(file="track.csv")
bitcoin <- c(track_file$Bitcoin)
anonycoin <- c(track_file$Anonycoin)

## Plot track
track <- read.csv(file="track_file.csv")
max_y <- max(track)
plot_colors <- c("blue", "red")
pdf("analysis-track.pdf")
plot(bitcoin, type="o", col=plot_colors[1], ylim=c(0, max_y), xlab="Transaction", ylab="Track")
box()
lines(anonycoin, type="o", pch=22, lty=2, col=plot_colors[2])
title(main="Bitcoin and Anonycoin Track", col.main="red", font.main=4)
legend(1, max_y, names(track_file), cex=0.8, col=plot_colors, pch=21:23,  lty=1:3)

## Statistics Bitcoin track
summary(bitcoin)
# Min.    1st Qu. Median  Mean    3rd Qu. Max. 
# 0.00    5.00    6.00    6.07    8.00   13.00 
mean(bitcoin)
# 6.070423
var(bitcoin)
# 5.60995
sd(bitcoin)
# 2.368533
median(bitcoin)
# 6
shapiro.test(rnorm(497, mean=6.070423, sd=2.368533))
# Shapiro-Wilk normality test
data:  rnorm(497, mean = 6.070423, sd = 2.368533)
W = 0.99346, p-value = 0.02995

## Statistics anonycoin track
summary(anonycoin)
# Min.    1st Qu. Median  Mean    3rd Qu. Max. 
# 0       0       0       0       0       0
mean(anonycoin)
# 0
var(anonycoin)
# 0
sd(anonycoin)
# 0
median(anonycoin)
# 0
shapiro.test(rnorm(497, mean=0, sd=0.01))
# Shapiro-Wilk normality test
# data:  rnorm(497, mean = 0, sd = 0.01)
# W = 0.99375, p-value = 0.03803

## Student's t distribution with 2 samples distincts
var.test(bitcoin, anonycoin)
# F test to compare two variances
# data:  bitcoin and anonycoin
# F = Inf, num df = 496, denom df = 496, p-value < 2.2e-16
# alternative hypothesis: true ratio of variances is not equal to 1
# 95 percent confidence interval:
# Inf Inf
# sample estimates:
# ratio of variances 
# Inf 
qf(0.95, 496, 496)
# 1.159344
t.test(bitcoin, anonycoin)
# Welch Two Sample t-test
# data:  bitcoin and anonycoin
# t = 57.137, df = 496, p-value < 2.2e-16
# alternative hypothesis: true difference in means is not equal to 0
# 95 percent confidence interval:
# 5.861680 6.279165
# sample estimates:
# mean of x mean of y 
# 6.070423  0.000000 
qt(0.975, 496)
# 1.964758

###############################################################################

## File time
time_file <- read.csv(file="time.csv")
bitcoin <- c(time_file$Bitcoin)
anonycoin <- c(time_file$Anonycoin)

## Plot time
max_y <- max(time_file)
plot_colors <- c("blue", "red")
pdf("analysis-time.pdf")
plot(bitcoin, type="o", col=plot_colors[1], ylim=c(0, max_y), xlab="Transaction", ylab="Time")
box()
lines(anonycoin, type="o", pch=22, lty=2, col=plot_colors[2])
title(main="Bitcoin and Anonycoin performance", col.main="red", font.main=4)
legend(1, max_y, names(time_file), cex=0.8, col=plot_colors, pch=21:23,  lty=1:3)

## Statistics Bitcoin time
summary(bitcoin)
# Min.    1st Qu. Median  Mean    3rd Qu. Max. 
# 0.1592  0.1647  0.1672  0.1680  0.1691  0.2234 
mean(bitcoin)
# 0.1680369
var(bitcoin)
# 4.591033e-05
sd(bitcoin)
# 0.006775716
median(bitcoin)
# 0.167183
shapiro.test(rnorm(497, mean=0.1680369, sd=0.006775716))
# Shapiro-Wilk normality test
# data:  rnorm(497, mean = 0.1680369, sd = 0.006775716)
# W = 0.994, p-value = 0.04687

## Statistics anonycoin time
summary(anonycoin)
# Min.    1st Qu. Median  Mean    3rd Qu. Max. 
# 0.3198  0.3271  0.3320  0.3361  0.3357  0.4862 
mean(anonycoin)
# 0.336092
var(anonycoin)
# 0.0004450458
sd(anonycoin)
# 0.02109611
median(anonycoin)
# 0.332019
shapiro.test(rnorm(497, mean=0.336092, sd=0.02109611))
# Shapiro-Wilk normality test
# data:  rnorm(497, mean = 0.336092, sd = 0.02109611)
# W = 0.99076, p-value = 0.003332

## Student's t distribution with 2 samples distincts
var.test(bitcoin, anonycoin)
# F test to compare two variances
# data:  bitcoin and anonycoin
# F = 0.10316, num df = 496, denom df = 496, p-value < 2.2e-16
# alternative hypothesis: true ratio of variances is not equal to 1
# 95 percent confidence interval:
# 0.08649235 0.12303647
# sample estimates:
# ratio of variances 
# 0.1031587 
qf(0.95, 496, 496)
# 1.159344
t.test(bitcoin, anonycoin)
# Welch Two Sample t-test
# data:  bitcoin and anonycoin
# t = -169.09, df = 597.26, p-value < 2.2e-16
# alternative hypothesis: true difference in means is not equal to 0
# 95 percent confidence interval:
# -0.1700070 -0.1661031
# sample estimates:
# mean of x mean of y 
# 0.1680369 0.3360920 
qt(0.975, 597.26)
# 1.963944
