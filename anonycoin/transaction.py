"""Anonycoin --- Bitcoin simulator with anonymity throught group signature

Copyright (c) 2016 Daniel Pimentel <d4n1@d4n1.org>

This file is part Anonycoin.

Anonycoin is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

Anonycoin is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Anonycoin.  If not, see <http://www.gnu.org/licenses/>.
"""

from script import Script


class Transaction:
    """Transaction Bitcoin simulator thread."""

    def __init__(self, inputs, outputs, btc):
        """Constructor.
        inputs - wallet input
        outputs - wallet output
        btc - btc amount
        sign - sign transaction
        """

        self.inputs = inputs
        self.outputs = outputs
        self.btc = btc
        self.sign = ""

    def make(self):
        """Make transacton."""
        
        script = Script(self.inputs.secret_key)
        self.inputs = self.inputs.hash_key
        self.outputs = self.outputs.hash_key
        self.sign = script.sign(self.inputs)
