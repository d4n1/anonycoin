"""Anonycoin --- Bitcoin simulator with anonymity throught group signature

Copyright (c) 2016 Daniel Pimentel <d4n1@d4n1.org>

This file is part Anonycoin.

Anonycoin is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

Anonycoin is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Anonycoin.  If not, see <http://www.gnu.org/licenses/>.
"""

from random import randrange, uniform
from itertools import permutations
from shutil import move
from time import clock
from pyfiglet import Figlet
from wallet import Wallet
from network import Publisher, Subscriber
from transaction import Transaction
from miner import Miner
from btc import btc_price
from group import Group


BTC = 1
WALLETS = 32
choice = 0

def menu():
    """Menu screen."""

    ascii = Figlet(font="slant")
    print(ascii.renderText("Anonycoin"))
    print("[0] Exit")
    print("[1] Anonycoin")
    print("[2] Bitcoin")
    print("[3] Price")
    print("[4] About")
    global choice
    choice = input("$ ")

def run():
    """Run Bitcoin simulator."""
    menu()
    
    while (choice != "0"):
        block = 10000

        if choice == "1":            
            for b in range(10):
                wallets = []

                for w in range(WALLETS):
                    wallet = Wallet()
                    wallets += [wallet]
                    
                publisher = Publisher()
                publisher.run()
                
                p = permutations(wallets, 2)
                g = Group()
            
                for t in p:
                    start_clock = clock()
                
                    transaction = Transaction(t[0], t[1], BTC)
                    transaction.make()
                    t[0].send(t[1], BTC)

                    subscriber = Subscriber()
                    subscriber.run()
                    publisher.broadcast("btc")

                    miner = Miner(transaction)

                    miner.pow(block)

                    t[0].sign = g.sign(t[0].hash_key)
                
                    miner.transaction.inputs = g.hash_key
                    miner.transaction.outputs = g.hash_key

                    miner.mining(b, "anonycoin.csv")

                    stop_clock = (clock() - start_clock) * 60
                
                    with open("anonycoin-time.csv", "a", encoding="utf-8") as f:
                        f.write(str(stop_clock) + "\n")

                    block += 1
                
        elif (choice == "2"):
            for b in range(10):
                wallets = []

                for w in range(WALLETS):
                    wallet = Wallet()
                    wallets += [wallet]

                publisher = Publisher()
                publisher.run()

                p = permutations(wallets, 2)
                miners = []
            
                for t in p:
                    start_clock = clock()
                
                    transaction = Transaction(t[0], t[1], BTC)
                    transaction.make()

                    t[0].send(t[1], BTC)

                    subscriber = Subscriber()
                    subscriber.run()
                    publisher.broadcast("btc")
            
                    miner = Miner(transaction)

                    miner.pow(block)
                    miner.mining(b, "bitcoin.csv")

                    stop_clock = (clock() - start_clock) * 60

                    with open("bitcoin-time.csv", "a", encoding="utf-8") as f:
                        f.write(str(stop_clock) + "\n")

                    block += 1
                    
        elif (choice == "3"):
            btc_price()
            
        elif (choice == "4"):
            print("""
            Anonycoin --- Bitcoin simulator with anonymity throught group signature

            Copyright (c) 2016 Daniel Pimentel <d4n1@d4n1.org>

            This file is part Anonycoin.

            Anonycoin is free software; you can redistribute it and/or modify it
            under the terms of the GNU General Public License as published by
            the Free Software Foundation; either version 3 of the License, or (at
            your option) any later version.

            Anonycoin is distributed in the hope that it will be useful, but
            WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU General Public License for more details.

            You should have received a copy of the GNU General Public License
            along with Anonycoin.  If not, see <http://www.gnu.org/licenses/>.
            """)
            
        menu()
    exit(0)


if __name__ == '__main__':
    run()
