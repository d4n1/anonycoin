"""Anonycoin --- Bitcoin simulator with anonymity throught group signature

Copyright (c) 2016 Daniel Pimentel <d4n1@d4n1.org>

This file is part Anonycoin.

Anonycoin is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

Anonycoin is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Anonycoin.  If not, see <http://www.gnu.org/licenses/>.
"""

from random import randrange
import zmq


class Publisher:
    """Network Bitcoin simulator server (ZMQ)."""

    def __init__(self):
        """Constructor.
        context - context zmq
        socket - socket p2p
        """

        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.PUB)

    def run(self):
        """Run thread."""

        self.socket.bind("tcp://*:4444")

    def broadcast(self, message):
        """Broadcast message.
        message - text message
        """

        message = bytes(message, 'utf8')
        self.socket.send(message)


class Subscriber:
    """Network Bitcoin simulator client."""

    def __init__(self):
        """Constructor.
        context - context zmq
        socket - socket p2p
        """

        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.SUB)

    def run(self):
        """Run thread,"""
        self.socket.connect("tcp://localhost:4444")
