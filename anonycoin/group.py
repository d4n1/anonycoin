"""Anonycoin --- Bitcoin simulator with anonymity throught group signature

Copyright(c) 2016 Daniel Pimentel <d4n1@d4n1.org>

This file is part Anonycoin.

Anonycoin is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

Anonycoin is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Anonycoin.  If not, see <http://www.gnu.org/licenses/>.
"""

from ecdsa import SigningKey


class Group:
    """Group signature."""

    def __init__(self):
        """Constructor.
        secret_key - group secret key
        public_key - group public key
        hash_key - group public key hash
        """

        self.secret_key = SigningKey.generate()
        self.public_key = self.secret_key.get_verifying_key()
        self.hash_key = self.public_key.to_string()

    def sign(self, message):
        """Signature.
        message - signature message
        """

        try:
            return self.secret_key.sign(bytes(message))
        except:
            return False

    def verify(self, sign, message):
        """Verify signature.
        sign - signature to be validated
        message - signature message
        """

        try:
            assert self.public_key.verify(sign, bytes(message))
            return True
        except:
            return False
