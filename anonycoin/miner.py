"""Anonycoin --- Bitcoin simulator with anonymity throught group signature

Copyright(c) 2016 Daniel Pimentel <d4n1@d4n1.org>

This file is part Anonycoin.

Anonycoin is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

Anonycoin is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Anonycoin.  If not, see <http://www.gnu.org/licenses/>.
"""

from datetime import datetime
import threading
import hashlib
from random import randrange
from blockchain import Blockchain


class Miner:
    """Miner Bitcoin simulator thread."""
    
    def __init__(self, transaction):
        """Constructor.
        transaction - transactor to be validated
        timestamp - mining timestamp
        difficulty - difficulty level to validate the transaction
        """

        self.transaction = transaction
        self.timestamp = datetime.now()
        self.difficulty = 0

    def pow(self, proof):
        """Proof-of-Work Bitcoin simulator using SHA-256."""

        secret = hashlib.sha256(str.encode(str(proof))).hexdigest()

        for i in range(1000000000):
            work = hashlib.sha256(str.encode(str(i))).hexdigest()
            if work == secret:
                self.difficulty = secret
                break

    def mining(self, block, simulation):
        """Mining Bitcoin simulator."""

        blockchain = Blockchain(block, self.transaction,
                                self.timestamp, self.difficulty)
        blockchain.blocking(simulation)

