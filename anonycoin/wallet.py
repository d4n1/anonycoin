"""Anonycoin --- Bitcoin simulator with anonymity throught group signature

Copyright (c) 2016 Daniel Pimentel <d4n1@d4n1.org>

This file is part Anonycoin.

Anonycoin is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

Anonycoin is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Anonycoin.  If not, see <http://www.gnu.org/licenses/>.
"""

from random import uniform, randrange
from ecdsa import SigningKey
from ipaddress import ip_address


class Wallet:
    """Wallet Bitcoin simulator thread ECDSA (SHA1)."""

    def __init__(self):
        """Constructor.
        secret_key - wallet secret key
        public_key - wallet public key
        hash_key - wallet public key hash
        btc - btc amount
        ip - ip address
        """

        self.secret_key = SigningKey.generate()
        self.public_key = self.secret_key.get_verifying_key()
        self.hash_key = self.public_key.to_string()
        self.btc = round(uniform(10, 100), 2)
        self.ip = ip_address(randrange(100))
        self.sign = ""

    def send(self, wallet, btc):
        """Send Bitcoin.
        wallet - wallet address
        btc - btc amount
        """

        wallet.receive(btc)
        self.btc - btc

    def receive(self, btc):
        """Receive Bitcoin.
        btc - btc amount
        """

        self.btc + btc
