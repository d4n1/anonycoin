## time

anonycoin <- read.csv(file="anonycoin-time.csv")
bitcoin <- read.csv(file="bitcoin-time.csv")
max_y <- max(anonycoin[,1])
plot_colors <- c("red", "blue")
pdf("analysis-time.pdf")
plot(anonycoin[,1], col=plot_colors[1], ylim=c(0, max_y), xlab="Numero de Transacoes", ylab="Tempo (minuto)")
box()
lines(bitcoin[,1], pch=22, lty=8, col=plot_colors[2])
title(main="Desempenho Bitcoin e Anonycoin", col.main="red", font.main=4)
legend(1, 3, c("Anonycoin", "Bitcoin"), cex=0.8, col=plot_colors, pch=21:23,  lty=1:3)

## statistic

anonycoin <- read.csv(file="anonycoin-time.csv")
bitcoin <- read.csv(file="bitcoin-time.csv")

summary(anonycoin)
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  8.169   9.283  10.076  10.120  10.930  14.066 

summary(bitcoin)
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  4.220   5.190   5.945   6.019   6.866   9.440 

mean(anonycoin)
[1] 10.12012

mean(bitcoin)
[1] 6.018846

var(anonycoin)
          time
time 0.9017807

var(bitcoin)
          time
time 0.9173767

var(bitcoin$time)
[1] 0.9173767

sd(bitcoin$time)
[1] 0.9577978

sd(anonycoin$time)
[1] 0.9496214

median(anonycoin$time)
[1] 10.07601

median(bitcoin$time)
[1] 5.94546

reg = lm(anonycoin$time~bitcoin$time)

summary(reg)

Call:
lm(formula = anonycoin$time ~ bitcoin$time)

Residuals:
     Min       1Q   Median       3Q      Max 
-2.11537 -0.18577  0.00065  0.18020  2.62312 

Coefficients:
             Estimate Std. Error t value Pr(>|t|)    
(Intercept)  4.429380   0.018262   242.5   <2e-16 ***
bitcoin$time 0.945487   0.002997   315.5   <2e-16 ***
---

Residual standard error: 0.2858 on 9918 degrees of freedom
Multiple R-squared:  0.9094,	Adjusted R-squared:  0.9094 
F-statistic: 9.956e+04 on 1 and 9918 DF,  p-value: < 2.2e-16

fitted(reg)

resid(reg)

shapiro.test(resid(reg)[0:5000])

pdf("qq-analysis.pdf")
qqnorm(resid(reg), xlab="Quantis teoricos", ylab="Quantis amostrais", main="Grafico Q-Q dos Residuos")
qqline(resid(reg), col=2)

pdf("rv-analysis.pdf")
plot(fitted(reg), resid(reg), xlab="Valores ajustados", ylab="Residuos", main="Residuos x Valores Ajustados")
abline(h=0, lty=2, col=2)

pdf("ro-analysis.pdf")
plot(resid(reg), type="o", xlab="Ordem", ylab="Residuos", main="Residuos x Ordem")
abline(h=0, lty=2, col=2)

pdf("c-analysis.pdf")
plot(anonycoin$time, bitcoin$time, xlab="Anonycoin", ylab="Bitcoin", main="Curva Analitica", abline(reg, col=2))